<?php

namespace HotWire\Console\Input;

interface InputInterface
{
    public function getArgument($name);
    public function getOption($name);
}
