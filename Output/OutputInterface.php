<?php

namespace HotWire\Console\Output;

interface OutputInterface
{
    public function getArgument($name);
    public function getOption($name);
}
