<?php

namespace HotWire\Console;

abstract class AbstractCommand
{
    private $name;
    private $description;
    private $arguments=array();
    private $options=array();

    abstract protected function configure();
    abstract protected function execute();

    /**
     * Gets the value of name.
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the value of name.
     *
     * @param mixed $name the name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Gets the value of description.
     *
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the value of description.
     *
     * @param mixed $description the description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    public function addArgument($name,$type,$description)
    {
        $this->arguments[]=new Argument($name, $type, $description);

        return $this;
    }
}
